package repository;


import models.Contact;

import java.util.ArrayList;
import java.util.List;

public class ContactRepository {
    private static final List<Contact> contacts = new ArrayList<>();

    public List<Contact> getAllContacts() {
        return contacts;
    }

    public void addContact(Contact contact) {
        contacts.add(contact);
    }
}