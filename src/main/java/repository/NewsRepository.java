package repository;


import models.News;

import java.util.ArrayList;
import java.util.List;

public class NewsRepository {
    private static final List<News> newsList = new ArrayList<>();

    public List<News> getAllNews() {
        return newsList;
    }

    public void addNews(News news) {
        newsList.add(news);
    }
}