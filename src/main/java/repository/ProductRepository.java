package repository;

import models.Product;

import java.util.ArrayList;
import java.util.List;

public class ProductRepository {
    private static final List<String> products = new ArrayList<>();

    public List<String> getAllProducts() {
        return products;
    }

    public void addProduct(String product) {
        products.add(product);
    }


}