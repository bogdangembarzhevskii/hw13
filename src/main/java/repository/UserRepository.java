package repository;

import models.User;

import java.util.ArrayList;
import java.util.List;

public class UserRepository {
    private static final List<String> users = new ArrayList<>();

    public List<String> getAllUsers() {
        return users;
    }

    public void addUser(String user) {
        users.add(user);
    }
}