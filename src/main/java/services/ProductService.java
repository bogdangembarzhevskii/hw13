package services;

import models.Product;
import repository.ProductRepository;

import java.util.List;

public class ProductService {
    private final ProductRepository productRepository = new ProductRepository();

    public List<String> getAllProducts() {
        return productRepository.getAllProducts();
    }

    public void addProduct(String product) {
        productRepository.addProduct(product);
    }
}