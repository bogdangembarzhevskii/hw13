package services;

import models.User;
import repository.UserRepository;

import java.util.List;

public class UserService {
    private final UserRepository userRepository = new UserRepository();

    public List<String> getAllUsers() {
        return userRepository.getAllUsers();
    }

    public void addUser(String user) {
        userRepository.addUser(user);
    }
}