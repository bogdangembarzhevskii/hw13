package servlets;

import models.Product;
import services.ProductService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/products")
public class ProductsServlet extends HttpServlet {
    private final ProductService productService = new ProductService();

    boolean begin = false;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        if (!begin) {
            productService.addProduct("Чеснок");
            productService.addProduct("Морковь");
            productService.addProduct("Бакланжан");

            begin = true;
        }

        List<String> products = productService.getAllProducts();
        req.setAttribute("products", products);

        req.getRequestDispatcher("jsp/products.jsp").forward(req, resp);
    }

}