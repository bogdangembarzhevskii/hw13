package servlets;

import models.User;
import repository.AccountsRepositoryImpl;
import services.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/users")
public class UsersServlet extends HttpServlet {
    private final UserService userService = new UserService();

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        List<String> users = userService.getAllUsers();
        req.getRequestDispatcher("jsp/users.jsp").forward(req, resp);
        // req.setAttribute("accounts", AccountsRepositoryImpl.accounts);

    }
}
