<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.util.List" %>
<%@ page import="models.Product" %>
<%@ page import="services.ProductService" %>

<%
    ProductService productService = new ProductService();
    List<String> products = productService.getAllProducts();
%>

<html>
<head>
    <title>Товары</title>
</head>
<body>
<h2>Список товаров</h2>
<ul>
    <% for (String product : products) { %>
    <li><%= product.toString() %></li>
    <% } %>
</ul>
</body>
</html>
