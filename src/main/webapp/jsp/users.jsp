<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.util.List" %>
<%@ page import="models.Product" %>
<%@ page import="services.ProductService" %>
<%@ page import="services.UserService" %>

<%
    UserService userService = new UserService();
    List<String> users = userService.getAllUsers();
%>

<html>
<head>
    <title>Пользователи</title>
</head>
<body>
<h2>Список пользователей: </h2>
<ul>
    <% for (String user : users) { %>
    <li><%= user.toString() %></li>
    <% } %>
</ul>
</body>
</html>
